﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using HortifrutiDias.Models;
using HortifrutiDias.Models.AccountViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace HortifrutiDias.Controllers
{

    [Route( "api/[controller]" )]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public AuthController( UserManager<ApplicationUser> userManager, IConfiguration configuration )
        {
            _userManager = userManager;
            _configuration = configuration;
        }

        // /register
        [Route( "register" )]
        [HttpPost]
        public async Task<ActionResult> InsertUser( [FromBody] RegisterViewModel model )
        {
            var user = new ApplicationUser
            {
                UserName = model.UserName,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var result = await _userManager.CreateAsync( user, model.Password );
            if( result.Succeeded )
            {
                //var role = model.IsManager ? "Manager" : "Employee";

                await _userManager.AddToRoleAsync( user, "Admin" );
            }

            return Ok( new { Username = user.UserName } );
        }

        //login
        [Route( "login" )]
        [HttpPost]
        public async Task<ActionResult> Login( [FromBody] LoginViewModel model )
        {
            var user = await _userManager.FindByNameAsync( model.Username );
            if( user != null && await _userManager.CheckPasswordAsync( user, model.Password ) )
            {
                var roles = await _userManager.GetRolesAsync( user );

                var claims = new[] {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.Role, roles.First()),
                    new Claim(ClaimTypes.NameIdentifier,user.Id)
                };

                var signinKey = new SymmetricSecurityKey( Encoding.UTF8.GetBytes( _configuration["Jwt:SigningKey"] ) );

                var expiryInHours = Convert.ToInt32( _configuration["Jwt:ExpiryInHours"] );

                var token = new JwtSecurityToken(
                  issuer: _configuration["Jwt:Involved"],
                  audience: _configuration["Jwt:Involved"],
                  expires: DateTime.UtcNow.AddHours( expiryInHours ),
                  signingCredentials: new SigningCredentials( signinKey, SecurityAlgorithms.HmacSha256 ),
                  claims: claims
                );

                return Ok(
                  new
                  {
                      token = new JwtSecurityTokenHandler().WriteToken( token ),
                      expiration = token.ValidTo
                  } );
            }
            return Unauthorized();
        }
    }

}