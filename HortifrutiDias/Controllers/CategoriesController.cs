﻿using System.Collections.Generic;
using System.Linq;
using HortifrutiDias.Data;
using HortifrutiDias.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HortifrutiDias.Controllers
{
    [Route( "api/[controller]" )]
    [Authorize]
    public class CategoriesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CategoriesController( ApplicationDbContext context )
        {
            this._context = context;
        }

        [HttpGet]
        public IEnumerable<Category> Get()
        {
            return this._context.Categories.OrderBy( o => o.Name );
        }
    }
}
