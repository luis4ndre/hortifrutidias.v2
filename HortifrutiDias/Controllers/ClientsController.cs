﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using HortifrutiDias.Data;
using HortifrutiDias.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HortifrutiDias.Controllers
{
    [Route( "api/[controller]" )]
    [Authorize]
    public class ClientsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ClientsController( ApplicationDbContext context )
        {
            this._context = context;
        }

        [HttpGet]
        public IEnumerable<Client> Get()
        {
            return this._context.Clients.ToList();
        }

        [HttpGet( "{id}" )]
        public Client Get( int id )
        {
            return this._context.Clients.First( f => f.Id.Equals( id ) );
        }

        [HttpPost]
        public HttpStatusCode Post( [FromBody]Client cliente )
        {
            if( this._context.Clients.Any( p => p.Name.ToUpper().Equals( cliente.Name.ToUpper() ) ) )
                return HttpStatusCode.Conflict;

            this._context.Add( cliente );
            this._context.SaveChanges();

            return HttpStatusCode.OK;
        }

        [HttpPut( "{id}" )]
        public HttpStatusCode Put( int id, [FromBody]Client client )
        {
            var dbClient = _context.Clients.Single( o => o.Id.Equals( id ) );

            dbClient.Name = client.Name;
            dbClient.Address = client.Address;
            dbClient.Contact = client.Contact;
            dbClient.Phone = client.Phone;
            dbClient.Excluded = client.Excluded;

            this._context.SaveChanges();

            return HttpStatusCode.OK;
        }
    }
}
