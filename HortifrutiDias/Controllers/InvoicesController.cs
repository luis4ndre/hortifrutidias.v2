﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using HortifrutiDias.Data;
using HortifrutiDias.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HortifrutiDias.Controllers
{
    [Route( "api/[controller]" )]
    [Authorize]
    public class InvoicesController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;

        public InvoicesController( UserManager<ApplicationUser> userManager, ApplicationDbContext context )
        {
            this._userManager = userManager;
            this._context = context;
        }

        [HttpGet( "byDate" )]
        public IEnumerable<Invoice> Get( bool today )
        {
            IEnumerable<Invoice> invoices = this._context.Invoices.Include( o => o.Client ).Include( o => o.InvoiceItems );

            if( today )
                invoices = invoices.Where( o => o.DeliveryDate.Date.Equals( DateTime.Today ) );

            return invoices.ToList();
        }

        [HttpGet( "{id?}" )]
        public Invoice Get( long id )
        {
            var invoice = this._context.Invoices.Include( c => c.Client ).Include( i => i.InvoiceItems ).ThenInclude( u => u.Unit ).Include( o => o.InvoiceItems ).ThenInclude( p => p.Product ).ThenInclude( o => o.Category ).Include( o => o.Client ).FirstOrDefault( f => f.Id.Equals( id ) ) ?? new Invoice { IsNew = true };

            invoice.FillProducts( this._context.Products.Include( p => p.Category ).ToList() );

            return invoice;
        }

        [HttpGet( "client/{id}" )]
        public IEnumerable<Invoice> GetByClient( long id )
        {
            return this._context.Invoices.Where( f => f.Client.Id.Equals( id ) );
        }

        [HttpPost]
        public async Task<HttpStatusCode> Post( [FromBody]Invoice invoice )
        {
            try
            {
                if( this._context.Invoices.Any( o => o.Client.Id == invoice.Client.Id && o.DeliveryDate.ToString( "MM/dd/yyyy" ) == invoice.DeliveryDate.ToString( "MM/dd/yyyy" ) ) )
                    return HttpStatusCode.Conflict;

                var user = await _userManager.FindByIdAsync( User.FindFirstValue( ClaimTypes.NameIdentifier ) );

                invoice.CreatedBy = user;

                this._context.Clients.Load();
                this._context.Products.Load();
                this._context.Units.Load();

                this._context.Invoices.Add( invoice );
                this._context.SaveChanges();

                return HttpStatusCode.Created;
            }
            catch( Exception e )
            {
                Console.WriteLine( e );
                throw;
            }
        }

        [HttpPut( "{id}" )]
        public HttpStatusCode Put( long id, [FromBody]Invoice invoice )
        {
            try
            {
                var dbInvoice = this._context.Invoices.FirstOrDefault( f => f.Id.Equals( id ) );

                if( dbInvoice == null )
                    return HttpStatusCode.NotFound;

                this._context.Clients.Load();
                this._context.Products.Load();
                this._context.Units.Load();
                this._context.Categories.Load();
                this._context.InvoiceItems.Load();

                dbInvoice.Excluded = invoice.Excluded;

                dbInvoice.Delivered = invoice.Delivered;

                dbInvoice.Paid = invoice.Paid;

                dbInvoice.DeliveryDate = invoice.DeliveryDate;

                if( invoice.Delivered )
                    dbInvoice.ClosedDate = DateTime.Now;
                else
                    dbInvoice.ClosedDate = null;

                dbInvoice.InvoiceItems.Clear();

                this._context.SaveChanges();

                dbInvoice.InvoiceItems = invoice.InvoiceItems;

                this._context.SaveChanges();

                return HttpStatusCode.OK;
            }
            catch( Exception e )
            {
                Console.WriteLine( e );
                return HttpStatusCode.InternalServerError;
            }
        }
    }
}