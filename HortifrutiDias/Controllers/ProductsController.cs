﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using HortifrutiDias.Data;
using HortifrutiDias.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HortifrutiDias.Controllers
{
    [Route( "api/[controller]" )]
    //[Authorize]
    public class ProductsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProductsController( ApplicationDbContext context )
        {
            this._context = context;
        }

        [HttpGet]
        public IEnumerable<Product> Get( bool filterExcluded, bool filterInvisible )
        {
            var products = this._context.Products.Include( o => o.Category ).OrderBy( o => o.Name ).ThenBy( o => o.Category );

            return products.Where( p => ( !filterExcluded || ( filterExcluded && !p.Excluded ) ) && ( !filterInvisible || ( filterInvisible && p.Visible ) ) );
        }

        [HttpGet( "{id}" )]
        public Product Get( int id )
        {
            return this._context.Products.Single( f => f.Id.Equals( id ) );
        }

        [HttpPost]
        public HttpStatusCode Post( [FromBody]Product product )
        {
            if( this._context.Products.Any( p => p.Name.ToUpper().Equals( product.Name.ToUpper() ) ) )
                return HttpStatusCode.Conflict;

            product.Name = product.Name.ToUpper();
            product.Category = _context.Categories.Single( o => o.Id.Equals( product.Category.Id ) );

            this._context.Add( product );
            this._context.SaveChanges();

            return HttpStatusCode.OK;
        }

        [HttpPut( "{id}" )]
        public HttpStatusCode Put( long id, [FromBody]Product product )
        {
            var dbProduct = this._context.Products.Single( o => o.Id.Equals( id ) );

            dbProduct.Name = product.Name.ToUpper();
            dbProduct.Category = this._context.Categories.Single( o => o.Id.Equals( product.Category.Id ) );
            dbProduct.Excluded = product.Excluded;
            dbProduct.Visible = product.Visible;

            this._context.SaveChanges();

            return HttpStatusCode.OK;
        }
    }
}
