﻿using System.Collections.Generic;
using System.Linq;
using HortifrutiDias.Data;
using HortifrutiDias.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HortifrutiDias.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class UnitsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public UnitsController( ApplicationDbContext context )
        {
            this._context = context;
        }

        [HttpGet]
        public IEnumerable<Unit> Get()
        {
            return this._context.Units.OrderBy(o=>o.Name);
        }
    }
}
