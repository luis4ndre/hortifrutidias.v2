interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Inicial',
    url: '/dashboard',
    icon: 'icon-speedometer'
  },
  {
    name: 'Clientes',
    url: '/clients',
    icon: 'fa fa-users'
  },
  {
    name: 'Pedidos',
    url: '/invoices',
    icon: 'fa fa-shopping-basket',
    children: [
      {
        name: 'Novo',
        url: '/invoices/invoices-add-edit',
        icon: 'fa fa-plus-circle'
      },
      {
        name: 'Histórico',
        url: '/invoices/invoices-history',
        icon: 'fa fa-history'
      }
    ]
  },
  {
    name: 'Produtos',
    url: '/products',
    icon: 'fa fa-cubes'
  }
];
