export interface IClient {
    id?: number;
    name?: string;
    address?: string;
    contact?: string;
    phone?: string;
    excluded?: boolean;

    isValid(): boolean;
}

export class Client implements IClient {
    constructor(
        public id?: number,
        public name?: string,
        public address?: string,
        public contact?: string,
        public phone?: string,
        public excluded?: boolean) { }

    isValid(): boolean {
        return this.name != null && this.address != null && this.contact != null && this.phone != null;
    }
}
