import { IProduct } from './product';
import { IClient } from './client';
import * as _ from 'lodash';

export interface IInvoice {
    id?: number;
    createdDate?: Date;
    deliveryDate?: Date;
    closedDate?: Date;
    invoiceItems?: IInvoiceItem[];
    client?: IClient;
    delivered: boolean;
    paid: boolean;
    excluded: boolean;
    number?: number;
    total?: number;
    isNew: boolean;

    calcTotal(): number;

    itemsCount(): number;

    readyToSave(): boolean;

    selectedItems(): IInvoiceItem[];

    isValid(): boolean;
}

export class Invoice implements IInvoice {
    constructor(
        public id?: number,
        public createdDate: Date = new Date(),
        public deliveryDate?: Date,
        public closedDate?: Date,
        public invoiceItems?: IInvoiceItem[],
        public client?: IClient,
        public delivered: boolean = false,
        public paid: boolean = false,
        public excluded: boolean = false,
        public number?: number,
        public total?: number,
        public isNew: boolean = true) { }

    calcTotal(): number {
        return _.sumBy(this.selectedItems(), function (o) {
            return o.price * o.amount;
        });
    }

    itemsCount(): number {
        return this.selectedItems().length;
    }

    selectedItems(): IInvoiceItem[] {
        return this.invoiceItems.filter(o => o.isValid());
    }

    readyToSave(): boolean {
        return this.itemsCount() <= 0 || this.deliveryDate == null || this.client == null;
    }

    isValid(): boolean {
        return this.itemsCount() > 0 && this.deliveryDate != null && this.client != null;
    }
}

export interface IInvoiceItem {
    id?: number;
    product?: IProduct;
    amount?: number;
    unit?: IUnit;
    price?: number;
    total?: number;

    isValid(): boolean;
    calcTotal(): number;
}

export class InvoiceItem implements IInvoiceItem {
    constructor(
        public id?: number,
        public product?: IProduct,
        public amount?: number,
        public unit?: IUnit,
        public price?: number) { }

    isValid(): boolean {
        return this.amount > 0 && this.price > 0 && this.unit != null;
    }

    calcTotal(): number {
        return this.amount * this.price;
    }
}

export interface IUnit {
    id?: number;
    name?: string;
    abbreviation?: string;
}
