export interface IProduct {
    id?: number;
    name?: string;
    category?: ICategory;
    excluded?: boolean;
    visible?: boolean;
}

export class Product implements IProduct {
    constructor(
        public id?: number,
        public name?: string,
        public category?: ICategory,
        public excluded?: boolean,
        public visible?: boolean) { }
}

export interface ICategory {
    id?: number;
    name?: string;
    sort?: number;
}

export class Category implements ICategory {
    constructor(
        public id?: number,
        public name?: string,
        public sort?: number) { }
}
