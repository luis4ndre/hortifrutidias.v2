import { TestBed, inject } from '@angular/core/testing';

import { HortifrutiDiasApiService } from './hortifruti-dias-api.service';

describe('HortifrutiDiasApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HortifrutiDiasApiService]
    });
  });

  it('should be created', inject([HortifrutiDiasApiService], (service: HortifrutiDiasApiService) => {
    expect(service).toBeTruthy();
  }));
});
