import {
  IInvoice,
  IUnit,
  Invoice,
  IInvoiceItem,
  InvoiceItem
} from '../domain/invoice';
import { IClient, Client } from '../domain/client';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, map, delay } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

import { IProduct, ICategory } from '../domain/product';

@Injectable({
  providedIn: 'root'
})
export class HortifrutiDiasApiService {
  private _productsURL = environment.apiUrl + 'products/';
  private _categoriesURL = environment.apiUrl + 'categories/';
  private _unitsURL = environment.apiUrl + 'units/';
  private _clientsURL = environment.apiUrl + 'clients/';
  private _invoicesURL = environment.apiUrl + 'invoices/';

  constructor(private http: HttpClient, private _router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'q=0.8;application/json;q=0.9'
    })
  };

  private getFromAPI<T>(
    url: string,
    id?: number,
    params?: HttpParams
  ): Observable<T> {
    if (id) {
      url = url + id;
    }

    return this.http
      .get<T>(url, {
        params: params
      })
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  getProducts(
    filterExcluded?: boolean,
    filterInvisible?: boolean
  ): Observable<IProduct[]> {
    return this.getFromAPI<IProduct[]>(
      this._productsURL,
      null,
      this.toHttpParams({
        filterExcluded: filterExcluded,
        filterInvisible: filterInvisible
      })
    );
  }

  getClients(): Observable<IClient[]> {
    return this.getFromAPI<IClient[]>(this._clientsURL).pipe(
      map(x => x.map((y: IClient) => Object.assign(new Client(), y)))
    );
  }

  getCategories(): Observable<ICategory[]> {
    return this.getFromAPI<ICategory[]>(this._categoriesURL);
  }

  getUnits(): Observable<IUnit[]> {
    return this.getFromAPI<IUnit[]>(this._unitsURL);
  }

  getInvoices(today: boolean): Observable<IInvoice[]> {
    return this.getFromAPI<IInvoice[]>(
      `${this._invoicesURL}byDate`,
      null,
      this.toHttpParams({
        today: today
      })
    );
  }

  getInvoice(id: number): Observable<IInvoice> {
    if (!id) {
      id = 0;
    }

    return this.getFromAPI<IInvoice>(this._invoicesURL, id).pipe(
      map(x => {
        x.invoiceItems = x.invoiceItems.map((i: IInvoiceItem) =>
          Object.assign(new InvoiceItem(), i)
        );
        return Object.assign(new Invoice(), x);
      })
    );
  }

  private saveOrUpdate(url: string, id: number, params: any): Observable<any> {
    let action: Observable<any>;

    if (id) {
      action = this.http.put(`${url}${id}`, params);
    } else {
      action = this.http.post(url, params);
    }

    return action.pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  saveOrUpdateProduct(product: IProduct): Observable<any> {
    return this.saveOrUpdate(this._productsURL, product.id, product);
  }

  saveOrUpdateClient(client: IClient): Observable<any> {
    return this.saveOrUpdate(this._clientsURL, client.id, client);
  }

  saveOrUpdateInvoice(invoice: IInvoice): Observable<any> {
    return this.saveOrUpdate(this._invoicesURL, invoice.id, invoice);
  }

  private toHttpParams(obj: Object): HttpParams {
    let params = new HttpParams();
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        const val = obj[key];
        if (val !== null && val !== undefined) {
          params = params.append(key, val.toString());
        }
      }
    }
    return params;
  }

  private handleError(error: Response) {
    return throwError(error.status + ': ' + error.statusText);
  }
}
