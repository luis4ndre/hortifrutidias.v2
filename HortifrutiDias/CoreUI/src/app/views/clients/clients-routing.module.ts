import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsComponent } from './clients.component';

import { AuthGuard } from '../../_http/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: ClientsComponent,
    data: {
      title: 'Clientes'
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
