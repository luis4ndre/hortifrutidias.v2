import { Component, OnInit } from '@angular/core';
import { IClient, Client } from '../../domain/client';
import { HortifrutiDiasApiService } from '../../services/hortifruti-dias-api.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  providers: [HortifrutiDiasApiService]
})
export class ClientsComponent implements OnInit {

  _loaded = false;
  _clients: IClient[] = [];
  _newClient: IClient = new Client();
  _displayDialog: boolean;
  _dialogTitle: string;

  constructor(private apiService: HortifrutiDiasApiService) { }

  ngOnInit(): void {
    this.getClients();
  }

  getClients(): void {
    this.apiService.getClients()
      .subscribe(
        resultArray => {
          this._clients = resultArray;
          this._loaded = true;
        },
        error => console.log('Error :: ' + error)
      );
  }

  toggleExcluded(client: IClient): void {
    client.excluded = !client.excluded;

    this.apiService.saveOrUpdateClient(client)
      .subscribe(
        result => console.log(result),
        error => {
          client.excluded = !client.excluded;
          console.log('Error :: ' + error);
        });
  }

  showDialogToAdd() {
    this._dialogTitle = 'Adicionar um novo Produto';

    this._newClient = new Client();
    this._displayDialog = true;
  }

  save() {
    const clients = [...this._clients];

    this._newClient.name = this._newClient.name.toUpperCase();

    if (this._newClient.id != null) {
      clients[this.findSelectedIndex()] = this._newClient;
    } else {
      clients.push(this._newClient);
    }

    this.apiService.saveOrUpdateClient(this._newClient)
      .subscribe(
        result => this._clients = clients,
        error => console.log('Error :: ' + error));

    this._newClient = new Client();
    this._displayDialog = false;
  }

  editClient(p: IClient) {
    this._dialogTitle = 'Editar o Cliente';

    this._newClient = this.clone(p);
    this._displayDialog = true;
  }

  clone(p: IClient): IClient {
    const client = { ...p };

    return client;
  }

  findSelectedIndex(): number {
    const updateItem = this._clients.find(item => item.id === this._newClient.id);

    return this._clients.indexOf(updateItem);
  }

  formIsInvalid(): boolean {
    let valid = true;
    if (!this._newClient.name) {
      valid = false;
    }

    if (!this._newClient.address) {
      valid = false;
    }

    if (!this._newClient.contact) {
      valid = false;
    }

    if (!this._newClient.phone) {
      valid = false;
    }

    return !valid;
  }
}
