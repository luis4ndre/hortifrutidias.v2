import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HortifrutiDiasApiService } from '../../../services/hortifruti-dias-api.service';
import { IInvoiceItem, IUnit, IInvoice, Invoice, InvoiceItem } from '../../../domain/invoice';
import * as _ from 'lodash';
import { forkJoin, Observable, Subject } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { ICategory, IProduct } from '../../../domain/product';
import { map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

defineLocale('pt-br', ptBrLocale);

@Component({
  selector: 'app-invoices-add-edit',
  templateUrl: './invoices-add-edit.component.html',
  styleUrls: ['./invoices-add-edit.component.scss']
})
export class InvoicesAddEditComponent implements OnInit {

  response$: Observable<any>;

  invoiceID: number;
  minDate: Date;
  maxDate: Date;
  allProducts: IProduct[];
  rowGroupMetadata: any;
  selectedClient: string;
  newItem: any = {};
  isOpen = false;
  Invoice: IInvoice;
  deliveryDate: Date;

  constructor(private apiService: HortifrutiDiasApiService,
    route: ActivatedRoute, localeService: BsLocaleService) {

    localeService.use('pt-br');

    this.invoiceID = route.snapshot.params['id'];

    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() - 5);
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() + 15);
  }

  ngOnInit() {

    this.response$ = forkJoin([
      this.apiService.getUnits(),
      this.apiService.getClients(),
      this.apiService.getInvoice(this.invoiceID)
    ]).pipe(
      map(([units, clients, invoice]) => {
        this.Invoice = invoice;

        if (!this.Invoice.isNew && this.Invoice.client != null) {
          this.selectedClient = this.Invoice.client.name;
        }

        this.Invoice.deliveryDate = new Date(this.Invoice.deliveryDate);

        return { units, clients };
      })
    );
  }

  productByCategory(category: ICategory): IProduct[] {
    return this.allProducts.filter(p => p.category.name === category.name);
  }

  onClientSelect(event: TypeaheadMatch): void {
    this.Invoice.client = event.item;
  }

  selectUnit(item: IInvoiceItem, c: IUnit, isNew: boolean) {
    item.unit = c;
    this.isOpen = false;
  }

  saveInvoice() {

    const newInvoice = _.cloneDeep(this.Invoice);
    newInvoice.invoiceItems = newInvoice.selectedItems();

    this.apiService.saveOrUpdateInvoice(newInvoice).subscribe(
      result => console.log(result),
      error => {
        console.log('Error :: ' + error);
      });
  }

  onDateChange(value: Date): void {
    this.Invoice.deliveryDate = value;
  }

  getClass(index: number, type: string) {
    let first = 'card-accent-primary';
    let second = 'card-accent-success';
    let third = 'card-accent-warning';
    let fourth = 'card-accent-danger';

    switch (type) {
      case 'background':
        first = 'bg-primary';
        second = 'bg-success';
        third = 'bg-warning';
        fourth = 'bg-danger';
        break;

      case 'text':
        first = 'text-primary';
        second = 'text-success';
        third = 'text-warning';
        fourth = 'text-danger';
        break;

      default:
        break;
    }

    if (index % 4 === 0) {
      return fourth;
    }

    if (index % 3 === 0) {
      return third;
    }

    if (index % 2 === 0) {
      return second;
    }

    return first;
  }
}
