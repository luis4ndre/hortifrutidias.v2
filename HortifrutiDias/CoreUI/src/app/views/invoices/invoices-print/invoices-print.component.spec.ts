import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicesPrintComponent } from './invoices-print.component';

describe('InvoicesPrintComponent', () => {
  let component: InvoicesPrintComponent;
  let fixture: ComponentFixture<InvoicesPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicesPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicesPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
