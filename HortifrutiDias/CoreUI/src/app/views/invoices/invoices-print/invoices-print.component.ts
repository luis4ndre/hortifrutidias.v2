import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HortifrutiDiasApiService } from '../../../services/hortifruti-dias-api.service';
import { IInvoice, IInvoiceItem, InvoiceItem } from '../../../domain/invoice';
import { IProduct } from '../../../domain/product';
import * as _ from 'lodash';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-invoices-print',
  templateUrl: './invoices-print.component.html',
  styleUrls: ['./invoices-print.component.scss'],
  providers: [HortifrutiDiasApiService]
})
export class InvoicesPrintComponent implements OnInit {
  invoiceID: number;
  processedInvoiceItems: any[];

  _invoiceLoaded = false;
  _invoice: IInvoice;
  _showAllProducts: boolean;
  _showPrices: boolean;
  _invoiceItemsLeft: object[] = [];
  _invoiceItemsRight: object[] = [];
  _allProducts: IProduct[];

  constructor(private apiService: HortifrutiDiasApiService,
    route: ActivatedRoute) {
    this.invoiceID = route.snapshot.params['id'];
  }

  ngOnInit() {
    forkJoin([
      this.apiService.getInvoice(this.invoiceID),
      this.apiService.getProducts(true, true)]
    ).subscribe(data => {
      this._invoice = data[0];

      this.buildTwoColumns(this._invoice.invoiceItems);

      this._allProducts = [];
      data[1].forEach(p => {
        if (!this._invoice.invoiceItems.some(x => x.product.id === p.id)) {
          this._allProducts.push(p);
        }
      });

      this._invoiceLoaded = true;
    });
  }

  buildTwoColumns(invoiceItems: InvoiceItem[]) {
    invoiceItems = _.sortBy(invoiceItems, ['product.category.sort', 'product.name']);

    const halfWayThough = Math.ceil(invoiceItems.length / 2);

    let chunk = _.chunk(invoiceItems, halfWayThough);

    let letfSize = _.size(chunk[0]) + _.size(_.groupBy(chunk[0], 'product.category.name'));
    let letfRight = _.size(chunk[1]) + _.size(_.groupBy(chunk[1], 'product.category.name'));

    let plus = 0;
    while (letfSize < letfRight) {
      plus++;
      chunk = _.chunk(invoiceItems, halfWayThough + plus);

      letfSize = _.size(chunk[0]) + _.size(_.groupBy(chunk[0], 'product.category.name'));
      letfRight = _.size(chunk[1]) + _.size(_.groupBy(chunk[1], 'product.category.name'));
    }

    this._invoiceItemsLeft = _.chain(chunk[0])
      .groupBy('product.category.name')
      .toPairs()
      .map(function (currentItem) {
        return _.zipObject(['key', 'value'], currentItem);
      })
      .value();

    this._invoiceItemsRight = _.chain(chunk[1])
      .groupBy('product.category.name')
      .toPairs()
      .map(function (currentItem) {
        return _.zipObject(['key', 'value'], currentItem);
      })
      .value();
  }

  toggleAllProducts(): void {
    const items = _.cloneDeep(this._invoice.invoiceItems);
    if (this._showAllProducts) {
      this._allProducts.forEach(a => {
        if (!items.some(x => x.product.category === a.category.name)) {
          items.push(new InvoiceItem(null, a, null, null, null));
        }
      });
    }

    this.buildTwoColumns(items);
  }
}
