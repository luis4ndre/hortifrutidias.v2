import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoicesAddEditComponent } from './invoices-add-edit/invoices-add-edit.component';
import { InvoicesPrintComponent } from './invoices-print/invoices-print.component';
import { InvoicesHistoryComponent } from './invoices-history/invoices-history.component';
import { AuthGuard } from '../../_http/auth.guard';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Pedidos'
    },
    canActivate: [AuthGuard],
    children: [
      {
        path: 'invoices-add-edit',
        component: InvoicesAddEditComponent,
        data: {
          title: 'Novo'
        }
      },
      {
        path: 'invoices-add-edit/:id',
        component: InvoicesAddEditComponent,
        data: {
          title: 'Editar'
        }
      },
      {
        path: 'invoices-history',
        component: InvoicesHistoryComponent,
        data: {
          title: 'Histórico'
        }
      },
      {
        path: 'invoices-print/:id',
        component: InvoicesPrintComponent,
        data: {
          title: 'Imprimir'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicesRoutingModule { }
