import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

import { HortifrutiDiasApiService } from '../../../services/hortifruti-dias-api.service';
import { IInvoice } from '../../../domain/invoice';


@Component({
  selector: 'app-invoices-table',
  templateUrl: 'invoices-table.component.html',
  providers: [HortifrutiDiasApiService]
})
export class InvoicesTableComponent implements OnInit {
  @Input() today = false;

  _loaded = false;
  _invoices: IInvoice[] = [];
  cols: any[];

  constructor(private apiService: HortifrutiDiasApiService, private router: Router) {
  }

  ngOnInit(): void {
    this.getInvoices(this.today);

    this.cols = [
      { field: 'number', header: '#Pedido' },
      { field: 'client.name', header: 'Cliente' },
      { field: 'deliveryDate', header: 'Data' },
      { field: 'total', header: 'Valor Total' }
    ];
  }

  getInvoices(today: boolean): void {
    this.apiService.getInvoices(today)
      .subscribe(
        resultArray => {
          this._invoices = resultArray;
          this._loaded = true;
        },
        error => console.log('Error :: ' + error)
      );
  }

  lookupRowStyleClass(invoice: IInvoice) {
    let classes = invoice.paid ? 'row-item-paid' : '';
    classes = classes + ' ' + (invoice.delivered ? 'row-item-delivered' : '');
    classes = classes + ' ' + (invoice.excluded ? 'row-item-excluded' : '');

    return classes;
  }

  editInvoice(invoice: IInvoice) {
    this.router.navigate(['./invoices/invoices-add-edit', invoice.id]);
  }

  printInvoice(invoice: IInvoice) {
    this.router.navigate(['./invoices/invoices-print', invoice.id]);
  }

  addNewInvoice() {
    this.router.navigate(['./invoices/invoices-add-edit']);
  }

  toggleExcluded(invoice: IInvoice) {
    invoice.excluded = !invoice.excluded;

    this.apiService.saveOrUpdateInvoice(invoice)
      .subscribe(
        result => console.log(result),
        error => {
          invoice.excluded = !invoice.excluded;
          console.log('Error :: ' + error);
        });
  }

  togglePaid(invoice: IInvoice) {
    invoice.paid = !invoice.paid;

    this.apiService.saveOrUpdateInvoice(invoice)
      .subscribe(
        result => console.log(result),
        error => {
          invoice.paid = !invoice.paid;
          console.log('Error :: ' + error);
        });
  }

  toggleDelivered(invoice: IInvoice) {
    invoice.delivered = !invoice.delivered;

    this.apiService.saveOrUpdateInvoice(invoice)
      .subscribe(
        result => console.log(result),
        error => {
          invoice.delivered = !invoice.delivered;
          console.log('Error :: ' + error);
        });
  }
}
