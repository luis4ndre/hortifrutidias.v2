import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoicesRoutingModule } from './invoices-routing.module';
import { InvoicesAddEditComponent } from './invoices-add-edit/invoices-add-edit.component';
import { InvoicesPrintComponent } from './invoices-print/invoices-print.component';
import { InvoicesHistoryComponent } from './invoices-history/invoices-history.component';
import { InvoicesTableComponent } from './invoices-table/invoices-table.component';
import { TableModule } from 'primeng/table';
import { CurrencyFormatPipe } from '../../pipes/currency-format.pipe';
import { LoadingComponent } from '../loading/loading.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  imports: [
    CommonModule,
    InvoicesRoutingModule,
    TableModule,
    ButtonModule,
    FormsModule,
    AutoCompleteModule,
    BsDatepickerModule.forRoot(),
    AccordionModule.forRoot(),
    PopoverModule.forRoot(),
    TypeaheadModule.forRoot()
  ],
  declarations: [
    InvoicesAddEditComponent,
    InvoicesPrintComponent,
    InvoicesHistoryComponent,
    InvoicesTableComponent,
    CurrencyFormatPipe,
    LoadingComponent
  ],
  exports: [InvoicesTableComponent]
})
export class InvoicesModule { }
