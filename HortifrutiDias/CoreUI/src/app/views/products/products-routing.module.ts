import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './products.component';

import { AuthGuard } from '../../_http/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: ProductsComponent,
    data: {
      title: 'Produtos'
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
