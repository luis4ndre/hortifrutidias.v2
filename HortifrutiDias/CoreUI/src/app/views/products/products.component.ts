import { Component, OnInit } from '@angular/core';
import { IProduct, Product } from '../../domain/product';
import { HortifrutiDiasApiService } from '../../services/hortifruti-dias-api.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  providers: [HortifrutiDiasApiService]
})
export class ProductsComponent implements OnInit {
  cols: any[];
  _products: IProduct[] = [];
  _newProduct: IProduct = new Product();
  _displayDialog: boolean;
  _dialogTitle: string;
  _categories: Object[] = [];
  _loaded = false;

  constructor(private apiService: HortifrutiDiasApiService) { }

  ngOnInit() {
    this.getProducts();

    this.getCategories();

    this.cols = [
      { field: 'name', header: 'Produto' },
      { field: 'category.name', header: 'Categoria' }
    ];
  }

  getProducts(): void {
    this.apiService.getProducts()
      .subscribe(
        resultArray => {
          this._products = resultArray;
          this._loaded = true;
        },
        error => console.log('Error :: ' + error)
      );
  }

  getCategories(): void {
    this.apiService.getCategories()
      .subscribe(
        resultArray => resultArray.forEach(rs => this._categories.push({ label: rs.name, value: rs })),
        error => console.log('Error :: ' + error)
      );
  }

  toggleExcluded(product: IProduct): void {
    product.excluded = !product.excluded;

    this.apiService.saveOrUpdateProduct(product)
      .subscribe(
        result => console.log(result),
        error => {
          product.excluded = !product.excluded;
          console.log('Error :: ' + error);
        });
  }

  toggleVisibility(product: IProduct): void {
    product.visible = !product.visible;

    this.apiService.saveOrUpdateProduct(product)
      .subscribe(
        result => console.log(result),
        error => {
          product.visible = !product.visible;
          console.log('Error :: ' + error);
        });
  }

  showDialogToAdd() {
    this._dialogTitle = 'Adicionar um novo Produto';

    this._newProduct = new Product();
    this._displayDialog = true;
  }

  save() {
    const products = [...this._products];

    this._newProduct.name = this._newProduct.name.toUpperCase();

    if (this._newProduct.id != null) {
      products[this.findSelectedIndex()] = this._newProduct;
    } else {
      products.push(this._newProduct);
    }

    this.apiService.saveOrUpdateProduct(this._newProduct)
      .subscribe(
        result => this._products = products,
        error => console.log('Error :: ' + error));

    this._newProduct = new Product();
    this._displayDialog = false;
  }

  editProduct(p: IProduct) {
    this._dialogTitle = 'Editar o Produto';

    this._newProduct = this.clone(p);
    this._displayDialog = true;
  }

  clone(p: IProduct): IProduct {
    const product = { ...p };

    return product;
  }

  findSelectedIndex(): number {
    const updateItem = this._products.find(item => item.id === this._newProduct.id);

    return this._products.indexOf(updateItem);
  }

  formIsInvalid(): boolean {
    let valid = true;
    if (!this._newProduct.name) {
      valid = false;
    }

    if (!this._newProduct.category) {
      valid = false;
    }

    return !valid;
  }

}
