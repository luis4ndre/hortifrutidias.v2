import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';

@NgModule({
  imports: [
    CommonModule,
    ProductsRoutingModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    DialogModule,
    ButtonModule
  ],
  declarations: [ProductsComponent]
})
export class ProductsModule { }
