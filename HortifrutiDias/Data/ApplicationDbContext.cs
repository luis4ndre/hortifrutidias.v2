﻿using System;
using HortifrutiDias.Data.Configuration;
using Microsoft.EntityFrameworkCore;
using HortifrutiDias.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace HortifrutiDias.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext( DbContextOptions options )
            : base( options )
        {
        }

        protected override void OnModelCreating( ModelBuilder builder )
        {
            base.OnModelCreating( builder );

            builder.ApplyConfiguration( new IdentityRoleConfiguration() );
            builder.ApplyConfiguration( new ApplicationUserConfiguration() );
            builder.ApplyConfiguration( new IdentityUserRoleConfiguration() );
            builder.ApplyConfiguration( new ClientConfiguration() );
            builder.ApplyConfiguration( new UnitConfiguration() );
            builder.ApplyConfiguration( new CategoryConfiguration() );
            builder.ApplyConfiguration( new ProductConfiguration() );
            builder.ApplyConfiguration( new InvoiceConfiguration() );
            builder.ApplyConfiguration( new InvoiceItemConfiguration() );
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceItem> InvoiceItems { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Unit> Units { get; set; }
    }
}
