﻿using System;
using System.Collections.Generic;
using System.Linq;
using HortifrutiDias.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HortifrutiDias.Data.Configuration
{
    public class ApplicationUserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure( EntityTypeBuilder<ApplicationUser> builder )
        {
            builder.ToTable( "AspNetUsers" );

            var appUser = new ApplicationUser
            {
                Id = "9D3C44C8-8E59-499D-A761-0D69648605FD",
                UserName = "luis4ndre",
                NormalizedUserName = "luis4ndre".ToUpper(),
                Email = "luis4ndre@gmail.com",
                NormalizedEmail = "luis4ndre@gmail.com".ToUpper(),
                TwoFactorEnabled = false,
                EmailConfirmed = true,
                FirstName = "Luis Andre",
                LastName = "Costa Gouveia"
            };

            var ph = new PasswordHasher<ApplicationUser>();
            appUser.PasswordHash = ph.HashPassword( appUser, "l23101630g" );

            builder.HasData(
                appUser
            );

            appUser = new ApplicationUser
            {
                Id = "1F6504BC-EFB8-4217-B318-F3D7731624E5",
                UserName = "felipedias",
                NormalizedUserName = "felipedias".ToUpper(),
                Email = "lfeliped_8@hotmail.com",
                NormalizedEmail = "lfeliped_8@hotmail.com".ToUpper(),
                TwoFactorEnabled = false,
                EmailConfirmed = true,
                FirstName = "Luis Felipe",
                LastName = "Dias Quintas"
            };

            ph = new PasswordHasher<ApplicationUser>();
            appUser.PasswordHash = ph.HashPassword( appUser, "123456" );

            builder.HasData(
                appUser
            );
        }
    }
}
