﻿using HortifrutiDias.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HortifrutiDias.Data.Configuration
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure( EntityTypeBuilder<Category> builder )
        {
            builder.ToTable( "Categories" );

            builder.Property( f => f.Id ).ValueGeneratedOnAdd();

            builder.HasData
            (
                new Category
                {
                    Id = 1,
                    Name = "Fruta",
                    Sort = 0
                },
                new Category
                {
                    Id = 2,
                    Name = "Legume",
                    Sort = 1
                }
                ,
                new Category
                {
                    Id = 3,
                    Name = "Verdura",
                    Sort = 2
                }
                ,
                new Category
                {
                    Id = 4,
                    Name = "Outro",
                    Sort = 3
                }
            );
        }
    }
}