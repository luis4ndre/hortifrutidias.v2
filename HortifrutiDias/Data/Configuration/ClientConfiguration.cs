﻿using HortifrutiDias.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HortifrutiDias.Data.Configuration
{
    public class ClientConfiguration : IEntityTypeConfiguration<Client>
    {
        public void Configure( EntityTypeBuilder<Client> builder )
        {
            builder.ToTable( "Clients" );

            builder.Property( f => f.Id ).ValueGeneratedOnAdd();

            builder.HasData
            (
                new Client
                {
                    Id = 1,
                    Address = "Rua padre, 123",
                    Agreement = "23432234",
                    Contact = "Jose",
                    Name = "Periquito",
                    Phone = "1138765878"
                },
                new Client
                {
                    Id = 2,
                    Address = "Rua Francisco, 465",
                    Agreement = "44432234",
                    Contact = "Maria",
                    Name = "Manuela",
                    Phone = "1138764456"
                },
                new Client
                {
                    Id = 3,
                    Address = "Rua Cardeal, 879",
                    Agreement = "55552234",
                    Contact = "Josefina",
                    Name = "Trigonela",
                    Phone = "1138889587"
                },
                new Client
                {
                    Id = 4,
                    Address = "Rua Teodoro, 888",
                    Agreement = "34552234",
                    Contact = "Carlos",
                    Name = "Pão de Ouro",
                    Phone = "1144489587"
                }
            );
        }
    }
}
