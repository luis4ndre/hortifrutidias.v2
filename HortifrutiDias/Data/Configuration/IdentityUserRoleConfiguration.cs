﻿using HortifrutiDias.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HortifrutiDias.Data.Configuration
{
    public class IdentityUserRoleConfiguration : IEntityTypeConfiguration<IdentityUserRole<string>>
    {
        public void Configure( EntityTypeBuilder<IdentityUserRole<string>> builder )
        {
            builder.ToTable( "AspNetUserRoles" );

            builder.HasData
            (
                new IdentityUserRole<string>
                {
                    UserId = "9D3C44C8-8E59-499D-A761-0D69648605FD",
                    RoleId = "1"
                },
                new IdentityUserRole<string>
                {
                    UserId = "1F6504BC-EFB8-4217-B318-F3D7731624E5",
                    RoleId = "2"
                }
            );
        }
    }
}