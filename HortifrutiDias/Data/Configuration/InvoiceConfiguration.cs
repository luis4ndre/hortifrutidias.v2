﻿using System;
using System.Collections.Generic;
using System.Linq;
using HortifrutiDias.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HortifrutiDias.Data.Configuration
{
    public class InvoiceConfiguration : IEntityTypeConfiguration<Invoice>
    {
        public void Configure( EntityTypeBuilder<Invoice> builder )
        {
            builder.ToTable( "Invoices" );

            builder.Property( f => f.Id ).ValueGeneratedOnAdd();

            builder.HasMany( x => x.InvoiceItems );
            builder.HasOne( x => x.Client );
            builder.HasOne( x => x.CreatedBy );

            //var invoices = new List<Invoice>();

            //var rnd = new Random();

            //var start = DateTime.Now;

            //for( var i = 1; i <= 10; i++ )
            //{
            //    if( start.AddMinutes( 5 ) < DateTime.Now )
            //        return;

            //    var date = DateTime.Now;

            //    if( i < 50 )
            //    {
            //        var month = rnd.Next( 1, 12 ) * -1;

            //        date = date.AddMonths( month );
            //    }

            //    invoices.Add( new Invoice
            //    {
            //        Id = i,
            //        ClientId = rnd.Next( 1, 4 ),
            //        CreatedDate = date,
            //        DeliveryDate = date.AddDays( i > 5 ? 0 : 1 ),
            //        Delivered = rnd.Next( 0, 1 ) != 0,
            //        UserId = "1F6504BC-EFB8-4217-B318-F3D7731624E5"
            //    } );
            //}

            //builder.HasData( invoices );
        }
    }
}
