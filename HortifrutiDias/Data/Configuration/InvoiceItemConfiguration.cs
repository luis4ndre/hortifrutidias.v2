﻿using System;
using System.Collections.Generic;
using System.Linq;
using HortifrutiDias.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HortifrutiDias.Data.Configuration
{
    public class InvoiceItemConfiguration : IEntityTypeConfiguration<InvoiceItem>
    {
        public void Configure( EntityTypeBuilder<InvoiceItem> builder )
        {
            builder.ToTable( "InvoiceItems" );

            builder.Property( f => f.Id ).ValueGeneratedOnAdd();

            builder.HasOne( x => x.Product );
            builder.HasOne( x => x.Unit );

            //var invoiceItem = new List<InvoiceItem>();

            //var rnd = new Random();

            //var invoiceItemId = 1;

            //for( var i = 1; i <= 10; i++ )
            //{
            //    int productId;

            //    var prods = new List<int>();

            //    var limit = rnd.Next( 1, 3);

            //    do
            //    {
            //        productId = rnd.Next( 1, 170 );

            //        if( !prods.Any() || !prods.Any( p => p.Equals( productId ) ) )
            //        {
            //            invoiceItem.Add( new InvoiceItem
            //            {
            //                Id = invoiceItemId,
            //                Amount = rnd.Next( 3, 25 ),
            //                Price = rnd.Next( 1, 50 ),
            //                ProductId = productId,
            //                UnitId = rnd.Next( 1, 4 ),
            //                InvoiceId = i
            //            } );

            //            prods.Add( productId );
            //            invoiceItemId += 1;
            //        }

            //    } while( prods.Count < limit );
            //}

            //builder.HasData( invoiceItem );

        }
    }
}
