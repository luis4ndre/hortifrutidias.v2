﻿using HortifrutiDias.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HortifrutiDias.Data.Configuration
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure( EntityTypeBuilder<Product> builder )
        {
            builder.ToTable( "Products" ).HasOne( x => x.Category );

            builder.Property( f => f.Id ).ValueGeneratedOnAdd();

            builder.HasData(
                new Product { Id = 1, Name = "ABACAXI HAVAÍ", CategoryId = 1 },
                new Product { Id = 2, Name = "CARAMBOLA", CategoryId = 1 },
                new Product { Id = 3, Name = "COCO VERDE", CategoryId = 1 },
                new Product { Id = 4, Name = "FIGO", CategoryId = 1 },
                new Product { Id = 5, Name = "FRAMBOESA", CategoryId = 1 },
                new Product { Id = 6, Name = "FR. DO CONDE/PINHA", CategoryId = 1 },
                new Product { Id = 7, Name = "LARANJA PERA", CategoryId = 1 },
                new Product { Id = 8, Name = "MAMÃO HAVAI (PAPAYA}", CategoryId = 1 },
                new Product { Id = 9, Name = "MARACUJÁ", CategoryId = 1 },
                new Product { Id = 10, Name = "MARMELO NACIONAL", CategoryId = 1 },
                new Product { Id = 11, Name = "MELANCIA", CategoryId = 1 },
                new Product { Id = 12, Name = "NECTARINA ESTRANG.", CategoryId = 1 },
                new Product { Id = 13, Name = "UVA RUBI", CategoryId = 1 },
                new Product { Id = 14, Name = "ABACATE", CategoryId = 1 },
                new Product { Id = 15, Name = "ABACAXI", CategoryId = 1 },
                new Product { Id = 16, Name = "ABIU", CategoryId = 1 },
                new Product { Id = 17, Name = "ACEROLA", CategoryId = 1 },
                new Product { Id = 18, Name = "AMEIXA ESTRANGEIRA", CategoryId = 1 },
                new Product { Id = 19, Name = "AMEIXA NACIONAL", CategoryId = 1 },
                new Product { Id = 20, Name = "BANANA MAÇÃ", CategoryId = 1 },
                new Product { Id = 21, Name = "CAJU", CategoryId = 1 },
                new Product { Id = 22, Name = "CEREJA ESTRANGEIRA", CategoryId = 1 },
                new Product { Id = 23, Name = "GOIABA", CategoryId = 1 },
                new Product { Id = 24, Name = "JACA", CategoryId = 1 },
                new Product { Id = 25, Name = "LARANJA", CategoryId = 1 },
                new Product { Id = 26, Name = "LIMÃO", CategoryId = 1 },
                new Product { Id = 27, Name = "LIMÃO TAITI", CategoryId = 1 },
                new Product { Id = 28, Name = "MAÇÃ NACIONAL FUJI", CategoryId = 1 },
                new Product { Id = 29, Name = "MAÇÃ ESTRANGEIRA", CategoryId = 1 },
                new Product { Id = 30, Name = "MAÇÃ ESTR. RED DEL", CategoryId = 1 },
                new Product { Id = 31, Name = "MANGA", CategoryId = 1 },
                new Product { Id = 32, Name = "MELÃO AMARELO", CategoryId = 1 },
                new Product { Id = 33, Name = "PERA NACIONAL", CategoryId = 1 },
                new Product { Id = 34, Name = "PÊSSEGO NACIONAL", CategoryId = 1 },
                new Product { Id = 35, Name = "PÊSSEGO ESTRANGEIRO", CategoryId = 1 },
                new Product { Id = 36, Name = "ROMÃ", CategoryId = 1 },
                new Product { Id = 37, Name = "TANGERINA MURCOT", CategoryId = 1 },
                new Product { Id = 38, Name = "UVA ITÁLIA", CategoryId = 1 },
                new Product { Id = 39, Name = "UVA NIÁGARA", CategoryId = 1 },
                new Product { Id = 40, Name = "ABACAXI PÉROLA", CategoryId = 1 },
                new Product { Id = 41, Name = "AMÊNDOA", CategoryId = 1 },
                new Product { Id = 42, Name = "ATEMÓIA", CategoryId = 1 },
                new Product { Id = 43, Name = "AVELÃ", CategoryId = 1 },
                new Product { Id = 44, Name = "BANANA", CategoryId = 1 },
                new Product { Id = 45, Name = "BANANA NANICA", CategoryId = 1 },
                new Product { Id = 46, Name = "BANANA PRATA", CategoryId = 1 },
                new Product { Id = 47, Name = "CAQUI", CategoryId = 1 },
                new Product { Id = 48, Name = "CASTANHA", CategoryId = 1 },
                new Product { Id = 49, Name = "CIDRA", CategoryId = 1 },
                new Product { Id = 50, Name = "DAMASCO ESTRANGEIRO", CategoryId = 1 },
                new Product { Id = 51, Name = "GRAVIOLA", CategoryId = 1 },
                new Product { Id = 52, Name = "GRAPE FRUIT", CategoryId = 1 },
                new Product { Id = 53, Name = "JABUTICABA", CategoryId = 1 },
                new Product { Id = 54, Name = "KIWI NACIONAL", CategoryId = 1 },
                new Product { Id = 55, Name = "KIWI ESTRANGEIRO", CategoryId = 1 },
                new Product { Id = 56, Name = "LARANJA LIMA", CategoryId = 1 },
                new Product { Id = 57, Name = "LICHIA", CategoryId = 1 },
                new Product { Id = 58, Name = "LIMA DA PÉRSIA", CategoryId = 1 },
                new Product { Id = 59, Name = "MAÇÃ NACIONAL", CategoryId = 1 },
                new Product { Id = 60, Name = "MAÇÃ NACIONAL GALA", CategoryId = 1 },
                new Product { Id = 61, Name = "MAÇÃ ESTRAN. GRANNY SMITH", CategoryId = 1 },
                new Product { Id = 62, Name = "MAMÃO FORMOSA", CategoryId = 1 },
                new Product { Id = 63, Name = "MANGOSTÃO", CategoryId = 1 },
                new Product { Id = 64, Name = "MARMELO ESTRANGEIRO", CategoryId = 1 },
                new Product { Id = 65, Name = "MEXERICA", CategoryId = 1 },
                new Product { Id = 66, Name = "MORANGO", CategoryId = 1 },
                new Product { Id = 67, Name = "NECTARINA NACIONAL", CategoryId = 1 },
                new Product { Id = 68, Name = "NÊSPERA", CategoryId = 1 },
                new Product { Id = 69, Name = "NOZES", CategoryId = 1 },
                new Product { Id = 70, Name = "PÊRA ESTRANGEIRA", CategoryId = 1 },
                new Product { Id = 71, Name = "QUINCAM", CategoryId = 1 },
                new Product { Id = 72, Name = "SERIGUELA", CategoryId = 1 },
                new Product { Id = 73, Name = "TAMARINDO", CategoryId = 1 },
                new Product { Id = 74, Name = "TANGERINA CRAVO", CategoryId = 1 },
                new Product { Id = 75, Name = "TANGERINA PONCAM", CategoryId = 1 },
                new Product { Id = 76, Name = "UVA ESTRANGEIRA", CategoryId = 1 },
                new Product { Id = 77, Name = "ABÓBORA MORANGA", CategoryId = 2 },
                new Product { Id = 78, Name = "ABÓBORA PAULISTA", CategoryId = 2 },
                new Product { Id = 79, Name = "ALCACHOFRA", CategoryId = 2 },
                new Product { Id = 80, Name = "BERINJELA CONSERVA", CategoryId = 2 },
                new Product { Id = 81, Name = "BERINJELA JAPONESA", CategoryId = 2 },
                new Product { Id = 82, Name = "CARÁ", CategoryId = 2 },
                new Product { Id = 83, Name = "CHUCHU", CategoryId = 2 },
                new Product { Id = 84, Name = "COGUMELO", CategoryId = 2 },
                new Product { Id = 85, Name = "ERVILHA COMUM", CategoryId = 2 },
                new Product { Id = 86, Name = "ERVILHA TORTA", CategoryId = 2 },
                new Product { Id = 87, Name = "FAVA", CategoryId = 2 },
                new Product { Id = 88, Name = "FEIJÃO CORADO", CategoryId = 2 },
                new Product { Id = 89, Name = "GENGIBRE", CategoryId = 2 },
                new Product { Id = 90, Name = "INHAME", CategoryId = 2 },
                new Product { Id = 91, Name = "JILÓ", CategoryId = 2 },
                new Product { Id = 92, Name = "MANDIOCA", CategoryId = 2 },
                new Product { Id = 93, Name = "MANDIOQUINHA", CategoryId = 2 },
                new Product { Id = 94, Name = "MELÃO SÃO CAETANO", CategoryId = 2 },
                new Product { Id = 95, Name = "PEPINO JAPONÊS", CategoryId = 2 },
                new Product { Id = 96, Name = "TAQUENOCO", CategoryId = 2 },
                new Product { Id = 97, Name = "ABÓBORA JAPONESA", CategoryId = 2 },
                new Product { Id = 98, Name = "ABOBRINHA ITALIANA", CategoryId = 2 },
                new Product { Id = 99, Name = "BATATA DOCE AMARELA", CategoryId = 2 },
                new Product { Id = 100, Name = "BATATA DOCE ROSADA", CategoryId = 2 },
                new Product { Id = 101, Name = "BERINJELA COMUM", CategoryId = 2 },
                new Product { Id = 102, Name = "CENOURA", CategoryId = 2 },
                new Product { Id = 103, Name = "MAXIXE", CategoryId = 2 },
                new Product { Id = 104, Name = "PEPINO CAIPIRA", CategoryId = 2 },
                new Product { Id = 105, Name = "PIMENTA VERMELHA", CategoryId = 2 },
                new Product { Id = 106, Name = "TOMATE SALADA", CategoryId = 2 },
                new Product { Id = 107, Name = "VAGEM MACARRÃO", CategoryId = 2 },
                new Product { Id = 108, Name = "ABÓBORA", CategoryId = 2 },
                new Product { Id = 109, Name = "ABÓBORA SECA", CategoryId = 2 },
                new Product { Id = 110, Name = "ABOBRINHA BRASILEIRA", CategoryId = 2 },
                new Product { Id = 111, Name = "BETERRABA", CategoryId = 2 },
                new Product { Id = 112, Name = "PEPINO COMUM", CategoryId = 2 },
                new Product { Id = 113, Name = "PIMENTA CAMBUCI", CategoryId = 2 },
                new Product { Id = 114, Name = "PIMENTÃO AMARELO", CategoryId = 2 },
                new Product { Id = 115, Name = "PIMENTÃO VERDE", CategoryId = 2 },
                new Product { Id = 116, Name = "PIMENTÃO VERMELHO", CategoryId = 2 },
                new Product { Id = 117, Name = "QUIABO", CategoryId = 2 },
                new Product { Id = 118, Name = "TOMATE", CategoryId = 2 },
                new Product { Id = 119, Name = "TOMATE CAQUI", CategoryId = 2 },
                new Product { Id = 120, Name = "ALFACE", CategoryId = 3 },
                new Product { Id = 121, Name = "CEBOLINHA", CategoryId = 3 },
                new Product { Id = 122, Name = "COUVE BRUXELAS", CategoryId = 3 },
                new Product { Id = 123, Name = "AGRIÃO", CategoryId = 3 },
                new Product { Id = 124, Name = "ALHO PORRÓ", CategoryId = 3 },
                new Product { Id = 125, Name = "CATALONHA", CategoryId = 3 },
                new Product { Id = 126, Name = "CENOURA COM FOLHAS", CategoryId = 3 },
                new Product { Id = 127, Name = "COENTRO", CategoryId = 3 },
                new Product { Id = 128, Name = "ERVA-DOCE", CategoryId = 3 },
                new Product { Id = 129, Name = "ESPINAFRE", CategoryId = 3 },
                new Product { Id = 130, Name = "GENGIBRE COM FOLHAS", CategoryId = 3 },
                new Product { Id = 131, Name = "GOBO", CategoryId = 3 },
                new Product { Id = 132, Name = "HORTELÃ", CategoryId = 3 },
                new Product { Id = 133, Name = "MILHO VERDE", CategoryId = 3 },
                new Product { Id = 134, Name = "MOYASHI", CategoryId = 3 },
                new Product { Id = 135, Name = "NABO", CategoryId = 3 },
                new Product { Id = 136, Name = "RABANETE", CategoryId = 3 },
                new Product { Id = 137, Name = "RÚCULA", CategoryId = 3 },
                new Product { Id = 138, Name = "SALSÃO", CategoryId = 3 },
                new Product { Id = 139, Name = "ACELGA", CategoryId = 3 },
                new Product { Id = 140, Name = "ALMEIRÃO", CategoryId = 3 },
                new Product { Id = 141, Name = "ASPARGO", CategoryId = 3 },
                new Product { Id = 142, Name = "BETERRABA COM FOLHAS", CategoryId = 3 },
                new Product { Id = 143, Name = "BRÓCOLIS", CategoryId = 3 },
                new Product { Id = 144, Name = "CHICÓRIA", CategoryId = 3 },
                new Product { Id = 145, Name = "COUVE", CategoryId = 3 },
                new Product { Id = 146, Name = "COUVE-FLOR", CategoryId = 3 },
                new Product { Id = 147, Name = "ENDÍVIA", CategoryId = 3 },
                new Product { Id = 148, Name = "ESCAROLA", CategoryId = 3 },
                new Product { Id = 149, Name = "FEIJÃO SOJA", CategoryId = 3 },
                new Product { Id = 150, Name = "FOLHA DE UVA", CategoryId = 3 },
                new Product { Id = 151, Name = "LOURO", CategoryId = 3 },
                new Product { Id = 152, Name = "MOSTARDA", CategoryId = 3 },
                new Product { Id = 153, Name = "ORÉGANO", CategoryId = 3 },
                new Product { Id = 154, Name = "PALMITO", CategoryId = 3 },
                new Product { Id = 155, Name = "REPOLHO", CategoryId = 3 },
                new Product { Id = 156, Name = "ALHO ESTRANGEIRO", CategoryId = 4 },
                new Product { Id = 157, Name = "AMENDOIM SEM CASCA", CategoryId = 4 },
                new Product { Id = 158, Name = "CEBOLA NACIONAL", CategoryId = 4 },
                new Product { Id = 159, Name = "BATATA NACIONAL", CategoryId = 4 },
                new Product { Id = 160, Name = "COCO SECO", CategoryId = 4 },
                new Product { Id = 161, Name = "ALHO NACIONAL", CategoryId = 4 },
                new Product { Id = 162, Name = "AMENDOIM COM CASCA", CategoryId = 4 },
                new Product { Id = 163, Name = "CANJICA", CategoryId = 4 },
                new Product { Id = 164, Name = "CEBOLA ESTRANGEIRA", CategoryId = 4 },
                new Product { Id = 165, Name = "MILHO PIPOCA NACIONAL", CategoryId = 4 },
                new Product { Id = 166, Name = "MILHO PIPOCA ESTRANGEIRO", CategoryId = 4 },
                new Product { Id = 167, Name = "OVOS BRANCOS", CategoryId = 4 },
                new Product { Id = 168, Name = "OVOS DE CODORNA", CategoryId = 4 },
                new Product { Id = 169, Name = "OVOS VERMELHOS", CategoryId = 4 },
                new Product { Id = 170, Name = "PINHÃO", CategoryId = 4 });
        }
    }
}