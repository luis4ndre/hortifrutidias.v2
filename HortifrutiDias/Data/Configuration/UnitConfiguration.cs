﻿using HortifrutiDias.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HortifrutiDias.Data.Configuration
{
    public class UnitConfiguration : IEntityTypeConfiguration<Unit>
    {
        public void Configure( EntityTypeBuilder<Unit> builder )
        {
            builder.ToTable( "Units" );

            builder.Property( f => f.Id ).ValueGeneratedOnAdd();

            builder.HasData
            (
                new Unit
                {
                    Id = 1,
                    Name = "Caixa",
                    Abbreviation = "cx"
                },
                new Unit
                {
                    Id = 2,
                    Name = "Unidade",
                    Abbreviation = "un"
                },
                new Unit
                {
                    Id = 3,
                    Name = "Maço",
                    Abbreviation = "mc"
                },
                new Unit
                {
                    Id = 4,
                    Name = "Saco",
                    Abbreviation = "sc"
                }
            );
        }
    }
}