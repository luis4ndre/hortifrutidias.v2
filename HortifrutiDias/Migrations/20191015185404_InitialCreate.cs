﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HortifrutiDias.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetIdentityRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetIdentityRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    Sort = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Agreement = table.Column<string>(nullable: true),
                    Contact = table.Column<string>(nullable: true),
                    Excluded = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    Abbreviation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetIdentityRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetIdentityRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetIdentityRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetIdentityRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    CategoryId = table.Column<long>(nullable: false),
                    Visible = table.Column<bool>(nullable: false),
                    Excluded = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Invoices",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DeliveryDate = table.Column<DateTime>(nullable: false),
                    ClosedDate = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<long>(nullable: false),
                    Delivered = table.Column<bool>(nullable: false),
                    Excluded = table.Column<bool>(nullable: false),
                    Paid = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Invoices_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Invoices_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InvoiceItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    InvoiceId = table.Column<long>(nullable: false),
                    ProductId = table.Column<long>(nullable: false),
                    UnitId = table.Column<long>(nullable: false),
                    Amount = table.Column<decimal>(nullable: true),
                    Price = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InvoiceItems_Invoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalTable: "Invoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InvoiceItems_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InvoiceItems_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetIdentityRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "3", "bfc923cd-42e2-4567-86c0-e031f767b418", "Employee", "EMPLOYEE" });

            migrationBuilder.InsertData(
                table: "AspNetIdentityRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "1", "71198c46-4486-4745-a9c3-edfbe2f3a83f", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetIdentityRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "2", "3a9d8777-274f-434d-a23b-7a8d77c4c4a1", "Manager", "MANAGER" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "9D3C44C8-8E59-499D-A761-0D69648605FD", 0, "e76468f4-9656-4949-b28d-88af601c259c", "luis4ndre@gmail.com", true, "Luis Andre", "Costa Gouveia", false, null, "LUIS4NDRE@GMAIL.COM", "LUIS4NDRE", "AQAAAAEAACcQAAAAEPQO9Ja3Ix/zfAML+Og2LlXNfZStXNF56+cr/1P5uLvW15TqIbcyj2vi22RtafP45Q==", null, false, null, false, "luis4ndre" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "1F6504BC-EFB8-4217-B318-F3D7731624E5", 0, "d5782b8c-860a-46aa-b989-b36c93ed33bf", "lfeliped_8@hotmail.com", true, "Luis Felipe", "Dias Quintas", false, null, "LFELIPED_8@HOTMAIL.COM", "FELIPEDIAS", "AQAAAAEAACcQAAAAENSpmTwtj6FwI7KLgHneUxMaNffoDccFTm0SXEyeUDiUy/YWfupP1bDU85oDg0I/VQ==", null, false, null, false, "felipedias" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name", "Sort" },
                values: new object[] { 3L, "Verdura", 2 });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name", "Sort" },
                values: new object[] { 4L, "Outro", 3 });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name", "Sort" },
                values: new object[] { 1L, "Fruta", 0 });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name", "Sort" },
                values: new object[] { 2L, "Legume", 1 });

            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Id", "Address", "Agreement", "Contact", "Excluded", "Name", "Phone" },
                values: new object[] { 1L, "Rua padre, 123", "23432234", "Jose", false, "Periquito", "1138765878" });

            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Id", "Address", "Agreement", "Contact", "Excluded", "Name", "Phone" },
                values: new object[] { 2L, "Rua Francisco, 465", "44432234", "Maria", false, "Manuela", "1138764456" });

            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Id", "Address", "Agreement", "Contact", "Excluded", "Name", "Phone" },
                values: new object[] { 4L, "Rua Teodoro, 888", "34552234", "Carlos", false, "Pão de Ouro", "1144489587" });

            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Id", "Address", "Agreement", "Contact", "Excluded", "Name", "Phone" },
                values: new object[] { 3L, "Rua Cardeal, 879", "55552234", "Josefina", false, "Trigonela", "1138889587" });

            migrationBuilder.InsertData(
                table: "Units",
                columns: new[] { "Id", "Abbreviation", "Name" },
                values: new object[] { 1L, "cx", "Caixa" });

            migrationBuilder.InsertData(
                table: "Units",
                columns: new[] { "Id", "Abbreviation", "Name" },
                values: new object[] { 2L, "un", "Unidade" });

            migrationBuilder.InsertData(
                table: "Units",
                columns: new[] { "Id", "Abbreviation", "Name" },
                values: new object[] { 3L, "mc", "Maço" });

            migrationBuilder.InsertData(
                table: "Units",
                columns: new[] { "Id", "Abbreviation", "Name" },
                values: new object[] { 4L, "sc", "Saco" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "1F6504BC-EFB8-4217-B318-F3D7731624E5", "2" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "9D3C44C8-8E59-499D-A761-0D69648605FD", "1" });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 169L, 4L, false, "OVOS VERMELHOS", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 109L, 2L, false, "ABÓBORA SECA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 110L, 2L, false, "ABOBRINHA BRASILEIRA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 111L, 2L, false, "BETERRABA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 112L, 2L, false, "PEPINO COMUM", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 113L, 2L, false, "PIMENTA CAMBUCI", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 114L, 2L, false, "PIMENTÃO AMARELO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 115L, 2L, false, "PIMENTÃO VERDE", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 116L, 2L, false, "PIMENTÃO VERMELHO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 117L, 2L, false, "QUIABO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 118L, 2L, false, "TOMATE", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 119L, 2L, false, "TOMATE CAQUI", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 120L, 3L, false, "ALFACE", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 121L, 3L, false, "CEBOLINHA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 122L, 3L, false, "COUVE BRUXELAS", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 123L, 3L, false, "AGRIÃO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 124L, 3L, false, "ALHO PORRÓ", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 125L, 3L, false, "CATALONHA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 108L, 2L, false, "ABÓBORA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 126L, 3L, false, "CENOURA COM FOLHAS", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 107L, 2L, false, "VAGEM MACARRÃO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 105L, 2L, false, "PIMENTA VERMELHA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 88L, 2L, false, "FEIJÃO CORADO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 89L, 2L, false, "GENGIBRE", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 90L, 2L, false, "INHAME", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 91L, 2L, false, "JILÓ", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 92L, 2L, false, "MANDIOCA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 93L, 2L, false, "MANDIOQUINHA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 94L, 2L, false, "MELÃO SÃO CAETANO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 95L, 2L, false, "PEPINO JAPONÊS", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 96L, 2L, false, "TAQUENOCO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 97L, 2L, false, "ABÓBORA JAPONESA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 98L, 2L, false, "ABOBRINHA ITALIANA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 99L, 2L, false, "BATATA DOCE AMARELA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 100L, 2L, false, "BATATA DOCE ROSADA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 101L, 2L, false, "BERINJELA COMUM", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 102L, 2L, false, "CENOURA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 103L, 2L, false, "MAXIXE", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 104L, 2L, false, "PEPINO CAIPIRA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 106L, 2L, false, "TOMATE SALADA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 127L, 3L, false, "COENTRO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 128L, 3L, false, "ERVA-DOCE", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 87L, 2L, false, "FAVA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 152L, 3L, false, "MOSTARDA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 153L, 3L, false, "ORÉGANO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 154L, 3L, false, "PALMITO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 155L, 3L, false, "REPOLHO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 156L, 4L, false, "ALHO ESTRANGEIRO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 157L, 4L, false, "AMENDOIM SEM CASCA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 158L, 4L, false, "CEBOLA NACIONAL", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 159L, 4L, false, "BATATA NACIONAL", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 160L, 4L, false, "COCO SECO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 161L, 4L, false, "ALHO NACIONAL", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 162L, 4L, false, "AMENDOIM COM CASCA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 163L, 4L, false, "CANJICA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 164L, 4L, false, "CEBOLA ESTRANGEIRA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 165L, 4L, false, "MILHO PIPOCA NACIONAL", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 166L, 4L, false, "MILHO PIPOCA ESTRANGEIRO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 167L, 4L, false, "OVOS BRANCOS", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 168L, 4L, false, "OVOS DE CODORNA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 151L, 3L, false, "LOURO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 150L, 3L, false, "FOLHA DE UVA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 149L, 3L, false, "FEIJÃO SOJA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 148L, 3L, false, "ESCAROLA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 130L, 3L, false, "GENGIBRE COM FOLHAS", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 131L, 3L, false, "GOBO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 132L, 3L, false, "HORTELÃ", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 133L, 3L, false, "MILHO VERDE", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 134L, 3L, false, "MOYASHI", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 135L, 3L, false, "NABO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 136L, 3L, false, "RABANETE", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 137L, 3L, false, "RÚCULA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 170L, 4L, false, "PINHÃO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 138L, 3L, false, "SALSÃO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 140L, 3L, false, "ALMEIRÃO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 141L, 3L, false, "ASPARGO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 142L, 3L, false, "BETERRABA COM FOLHAS", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 143L, 3L, false, "BRÓCOLIS", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 144L, 3L, false, "CHICÓRIA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 145L, 3L, false, "COUVE", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 146L, 3L, false, "COUVE-FLOR", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 147L, 3L, false, "ENDÍVIA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 139L, 3L, false, "ACELGA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 129L, 3L, false, "ESPINAFRE", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 86L, 2L, false, "ERVILHA TORTA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 84L, 2L, false, "COGUMELO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 24L, 1L, false, "JACA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 25L, 1L, false, "LARANJA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 26L, 1L, false, "LIMÃO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 27L, 1L, false, "LIMÃO TAITI", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 28L, 1L, false, "MAÇÃ NACIONAL FUJI", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 29L, 1L, false, "MAÇÃ ESTRANGEIRA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 30L, 1L, false, "MAÇÃ ESTR. RED DEL", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 31L, 1L, false, "MANGA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 32L, 1L, false, "MELÃO AMARELO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 33L, 1L, false, "PERA NACIONAL", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 34L, 1L, false, "PÊSSEGO NACIONAL", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 35L, 1L, false, "PÊSSEGO ESTRANGEIRO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 36L, 1L, false, "ROMÃ", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 37L, 1L, false, "TANGERINA MURCOT", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 38L, 1L, false, "UVA ITÁLIA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 39L, 1L, false, "UVA NIÁGARA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 40L, 1L, false, "ABACAXI PÉROLA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 23L, 1L, false, "GOIABA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 22L, 1L, false, "CEREJA ESTRANGEIRA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 21L, 1L, false, "CAJU", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 20L, 1L, false, "BANANA MAÇÃ", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 2L, 1L, false, "CARAMBOLA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 3L, 1L, false, "COCO VERDE", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 4L, 1L, false, "FIGO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 5L, 1L, false, "FRAMBOESA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 6L, 1L, false, "FR. DO CONDE/PINHA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 7L, 1L, false, "LARANJA PERA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 8L, 1L, false, "MAMÃO HAVAI (PAPAYA}", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 9L, 1L, false, "MARACUJÁ", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 41L, 1L, false, "AMÊNDOA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 10L, 1L, false, "MARMELO NACIONAL", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 12L, 1L, false, "NECTARINA ESTRANG.", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 13L, 1L, false, "UVA RUBI", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 14L, 1L, false, "ABACATE", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 15L, 1L, false, "ABACAXI", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 16L, 1L, false, "ABIU", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 17L, 1L, false, "ACEROLA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 18L, 1L, false, "AMEIXA ESTRANGEIRA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 19L, 1L, false, "AMEIXA NACIONAL", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 11L, 1L, false, "MELANCIA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 85L, 2L, false, "ERVILHA COMUM", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 42L, 1L, false, "ATEMÓIA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 44L, 1L, false, "BANANA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 67L, 1L, false, "NECTARINA NACIONAL", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 68L, 1L, false, "NÊSPERA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 69L, 1L, false, "NOZES", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 70L, 1L, false, "PÊRA ESTRANGEIRA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 71L, 1L, false, "QUINCAM", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 72L, 1L, false, "SERIGUELA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 73L, 1L, false, "TAMARINDO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 74L, 1L, false, "TANGERINA CRAVO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 75L, 1L, false, "TANGERINA PONCAM", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 76L, 1L, false, "UVA ESTRANGEIRA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 77L, 2L, false, "ABÓBORA MORANGA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 78L, 2L, false, "ABÓBORA PAULISTA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 79L, 2L, false, "ALCACHOFRA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 80L, 2L, false, "BERINJELA CONSERVA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 81L, 2L, false, "BERINJELA JAPONESA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 82L, 2L, false, "CARÁ", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 83L, 2L, false, "CHUCHU", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 66L, 1L, false, "MORANGO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 65L, 1L, false, "MEXERICA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 64L, 1L, false, "MARMELO ESTRANGEIRO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 63L, 1L, false, "MANGOSTÃO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 45L, 1L, false, "BANANA NANICA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 46L, 1L, false, "BANANA PRATA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 47L, 1L, false, "CAQUI", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 48L, 1L, false, "CASTANHA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 49L, 1L, false, "CIDRA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 50L, 1L, false, "DAMASCO ESTRANGEIRO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 51L, 1L, false, "GRAVIOLA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 52L, 1L, false, "GRAPE FRUIT", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 43L, 1L, false, "AVELÃ", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 53L, 1L, false, "JABUTICABA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 55L, 1L, false, "KIWI ESTRANGEIRO", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 56L, 1L, false, "LARANJA LIMA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 57L, 1L, false, "LICHIA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 58L, 1L, false, "LIMA DA PÉRSIA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 59L, 1L, false, "MAÇÃ NACIONAL", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 60L, 1L, false, "MAÇÃ NACIONAL GALA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 61L, 1L, false, "MAÇÃ ESTRAN. GRANNY SMITH", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 62L, 1L, false, "MAMÃO FORMOSA", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 54L, 1L, false, "KIWI NACIONAL", true });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Excluded", "Name", "Visible" },
                values: new object[] { 1L, 1L, false, "ABACAXI HAVAÍ", true });

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetIdentityRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceItems_InvoiceId",
                table: "InvoiceItems",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceItems_ProductId",
                table: "InvoiceItems",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceItems_UnitId",
                table: "InvoiceItems",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_ClientId",
                table: "Invoices",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_UserId",
                table: "Invoices",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "InvoiceItems");

            migrationBuilder.DropTable(
                name: "AspNetIdentityRoles");

            migrationBuilder.DropTable(
                name: "Invoices");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Units");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
