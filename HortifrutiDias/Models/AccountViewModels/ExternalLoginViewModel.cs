using System.ComponentModel.DataAnnotations;

namespace HortifrutiDias.Models.AccountViewModels
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
