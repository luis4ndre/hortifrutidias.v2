﻿using System.ComponentModel.DataAnnotations;

namespace HortifrutiDias.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
