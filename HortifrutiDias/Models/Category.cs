﻿using System.ComponentModel.DataAnnotations;

namespace HortifrutiDias.Models
{
    public class Category
    {
        public Category() { }
        public Category( string name, int sort )
        {
            this.Name = name;
            this.Sort = sort;
        }

        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public int Sort { get; set; }
    }
}
