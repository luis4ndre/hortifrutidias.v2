﻿using System.ComponentModel.DataAnnotations;

namespace HortifrutiDias.Models
{
    public class Client
    {
        public Client() { }

        public Client(string name, string address, string phone, string agreement, string contact)
        {
            this.Name = name;
            this.Address = address;
            this.Phone = phone;
            this.Agreement = agreement;
            this.Contact = contact;
        }

        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Agreement { get; set; }
        public string Contact { get; set; }
        public bool Excluded { get; set; }
    }
}