﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace HortifrutiDias.Models
{
    public class Invoice
    {
        public Invoice()
        {
            this.InvoiceItems = new List<InvoiceItem>();
        }

        [Key]
        public long Id { get; set; }
        public string UserId { get; set; }
        [ForeignKey( nameof( UserId ) )]
        public ApplicationUser CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime? ClosedDate { get; set; }
        public IList<InvoiceItem> InvoiceItems { get; set; }
        public long ClientId { get; set; }
        [ForeignKey( nameof( ClientId ) )]
        public Client Client { get; set; }
        public bool Delivered { get; set; }
        public bool Excluded { get; set; }
        public bool Paid { get; set; }
        [NotMapped]
        public bool IsNew { get; set; }
        public string Number => this.CreatedDate.ToString( "yyMMddHHmm" );

        public decimal Total => this.InvoiceItems.Any() ? this.InvoiceItems.Sum( s => s.Total ) : 0;

        public void FillProducts( List<Product> products )
        {
            products.ForEach( p =>
             {
                 if( this.InvoiceItems.All( i => i.Product.Id != p.Id ) )
                     this.InvoiceItems.Add( new InvoiceItem( p ) );
             } );

            this.InvoiceItems = this.InvoiceItems.OrderBy( o => o.Product.Category.Sort ).ThenBy( o => o.Product.Name ).ToList();
        }
    }
}