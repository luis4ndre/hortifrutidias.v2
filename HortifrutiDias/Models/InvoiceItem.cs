﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HortifrutiDias.Models
{
    public class InvoiceItem
    {
        public InvoiceItem() { }

        public InvoiceItem( Product product, Unit unit = null, decimal? amount = null, decimal? value = null )
        {
            this.Product = product;
            this.Unit = unit;
            this.Amount = amount;
            this.Price = value;
        }

        [Key]
        public long Id { get; set; }
        public long InvoiceId { get; set; }
        public long ProductId { get; set; }
        [ForeignKey( nameof( ProductId ) )]
        public Product Product { get; set; }
        public long UnitId { get; set; }
        [ForeignKey( nameof( UnitId ) )]
        public Unit Unit { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Price { get; set; }
        public decimal Total => ( this.Price.HasValue && this.Amount.HasValue ) ? this.Price.Value * this.Amount.Value : 0M;
    }
}
