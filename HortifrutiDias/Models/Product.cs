﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HortifrutiDias.Models
{
    public class Product
    {
        public Product()
        {
            this.Visible = true;
        }

        public Product( string name, Category category ) : this()
        {
            this.Name = name;
            this.Category = category;
        }

        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public long CategoryId { get; set; }
        [ForeignKey( nameof( CategoryId ) )]
        public Category Category { get; set; }
        public bool Visible { get; set; }
        public bool Excluded { get; set; }
    }
}