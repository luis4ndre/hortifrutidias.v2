﻿using System.ComponentModel.DataAnnotations;

namespace HortifrutiDias.Models
{
    public class Unit
    {
        public Unit() { }
        public Unit( string name, string abv )
        {
            this.Name = name;
            this.Abbreviation = abv;
        }

        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
    }
}
